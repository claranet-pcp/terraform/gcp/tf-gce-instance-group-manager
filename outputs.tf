output "instance_group" {
  value = "${google_compute_instance_group_manager.instance_group_manager.instance_group}"
}

output "link" {
  value = "${google_compute_instance_group_manager.instance_group_manager.self_link}"
}
